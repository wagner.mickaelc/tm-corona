import logo from './logo.svg';
import './App.css';
import Liste from './Liste.js';
import data from "./fakedata.json";

console.log("Message 1")
const MonComposant = () => {
  console.log("Message 2");
  return (<div>Hello world</div>);
};

function App() {
  return <Liste data={data} />;
}

export default App;