const Liste = ({ data }) => {
    return (
      <ol>
        {data.Countries.map((element) => (
          <li>
            {element.Country}
          </li>
        ))}
      </ol>
    );
  };
  

export default Liste

/*
const Liste = () => {
    return (
        <ol>
            <li>Element A</li>
            <li>Element B</li>
            <li>Element C</li>
        </ol>
    );
  };
  */